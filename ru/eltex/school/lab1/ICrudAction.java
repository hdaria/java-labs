package ru.eltex.school.lab1;

public interface ICrudAction {

    void read();

    void update();

    void delete();
}
