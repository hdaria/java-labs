package ru.eltex.school.lab1;

import java.util.Random;
import java.util.Scanner;

public class Mobile extends Goods {

    Scanner scan = new Scanner(System.in);
    Random random = new Random();

    private String casesType[] = {"Classic", "Clamshell"};
    private int typeOfCase;


    Mobile() {
        super();
        typeOfCase = random.nextInt(casesType.length);
    }


    @Override
    public void read() {
        super.read();
        System.out.println("Тип корпуса телефона: " + casesType[this.typeOfCase]);
    }

    @Override
    public void update() {
        super.update();
        System.out.println("Введите тип корпуса телефона (0 - Classic, 1 - Clamshell):");
        this.typeOfCase = scan.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        this.typeOfCase = -1;
    }
}
