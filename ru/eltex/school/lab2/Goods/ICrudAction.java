package ru.eltex.school.lab2.Goods;

public interface ICrudAction {

    void read();

    void update();

    void delete();
}
