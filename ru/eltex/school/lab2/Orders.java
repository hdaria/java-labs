package ru.eltex.school.lab2;

import java.util.*;

public class Orders {

    List<Order> orderList;
    Map<Date, Order> timeOrders;

    Orders() {
        orderList = new ArrayList<>();
        timeOrders = new TreeMap<>();
    }

    public boolean add(Order order) {
        timeOrders.put(order.getTime(), order);
        return orderList.add(order);
    }

    public boolean buy(ShoppingCart cart, Credentials user) {
        Order ord = new Order(cart, user);
        return orderList.add(ord);
    }

    public void check() {
        Iterator<Order> it = orderList.iterator();
        while (it.hasNext()) {
            Order ord = it.next();
            if ((System.currentTimeMillis() > (ord.getWaitTime() + ord.getCreateTime())) || ord.getStatus() == Status.DONE) {
                it.remove();
            }

        }
    }

    public void showOrders() {
        for (Order ord : orderList) {
            ord.show();
        }
    }

}