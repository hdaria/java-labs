package ru.eltex.school.lab2;

import ru.eltex.school.lab2.Goods.Goods;

import java.util.*;

public class ShoppingCart {

    List<Goods> cart;
    Set<UUID> ID;

    public ShoppingCart() {
        this.cart = new LinkedList<>();
        this.ID = new HashSet<>();
    }

    public boolean add(Goods product) {
        ID.add(product.getID());
        return cart.add(product);
    }

    public boolean delete(Goods product) {
        ID.remove(product.getID());
        return cart.remove(product);
    }

    public boolean searchCart(UUID id) {
        return ID.contains(id);
    }

    public void showCart() {
        for (Goods prod : cart) {
            prod.read();
        }
    }

}
