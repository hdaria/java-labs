package ru.eltex.school.lab4.AChecks;

import ru.eltex.school.lab4.Orders.Orders;
import ru.eltex.school.lab4.Orders.Order;

public abstract class ACheck implements Runnable {

    public boolean frun = true;
    protected Orders<Order> orders;

    public synchronized void setOrders(Orders<Order> orders) {
        this.orders = orders;
    }

}
