package ru.eltex.school.lab4.Goods;

public interface ICrudAction {

    void read();

    void update();

    void delete();
}
