package ru.eltex.school.lab4.Orders;

import java.util.*;

public class Orders<T extends Order> {

    private List<T> orderList;
    Map<Date, T> timeOrders;

    public Orders() {
        orderList = new ArrayList<>();
        timeOrders = new TreeMap<>();
    }

    public boolean add(T order) {
        timeOrders.put(order.getTime(), order);
        return orderList.add(order);
    }

    public boolean buy(ShoppingCart cart, Credentials user) {
        T ord = (T) new Order(cart, user);
        return orderList.add(ord);
    }

    public void checkWait() {
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if (ord.getStatus() == Status.WAIT) {
                ord.setStatus(Status.DONE);
            }

        }
    }

    public void checkDone() {
        Iterator<T> it = orderList.iterator();
        while (it.hasNext()) {
            T ord = it.next();
            if (ord.getStatus() == Status.DONE) {
                it.remove();
            }

        }
    }


    public void showOrders() {
        for (T ord : orderList) {
            ord.show();
        }
    }

    public List<T> getOrder() {
        return this.orderList;
    }

}